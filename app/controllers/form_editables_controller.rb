class FormEditablesController < ApplicationController
  before_action :set_form_editable, only: [:show, :edit, :update, :destroy]

  # GET /form_editables
  # GET /form_editables.json
  def index
    @form_editables = FormEditable.all
  end

  # GET /form_editables/1
  # GET /form_editables/1.json
  def show
  end

  # GET /form_editables/new
  def new
    @form_editable = FormEditable.new
  end

  # GET /form_editables/1/edit
  def edit
  end

  # POST /form_editables
  # POST /form_editables.json
  def create
    @form_editable = FormEditable.new(form_editable_params)

    respond_to do |format|
      if @form_editable.save
        format.html { redirect_to @form_editable, notice: 'Form editable was successfully created.' }
        format.json { render :show, status: :created, location: @form_editable }
      else
        format.html { render :new }
        format.json { render json: @form_editable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /form_editables/1
  # PATCH/PUT /form_editables/1.json
  def update
    respond_to do |format|
      if @form_editable.update(form_editable_params)
        format.html { redirect_to @form_editable, notice: 'Form editable was successfully updated.' }
        format.json { render :show, status: :ok, location: @form_editable }
      else
        format.html { render :edit }
        format.json { render json: @form_editable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /form_editables/1
  # DELETE /form_editables/1.json
  def destroy
    @form_editable.destroy
    respond_to do |format|
      format.html { redirect_to form_editables_url, notice: 'Form editable was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form_editable
      @form_editable = FormEditable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def form_editable_params
      params.require(:form_editable).permit(:down_payment_percentage, :max_payment_amount_limit, :months, :interest)
    end
end
