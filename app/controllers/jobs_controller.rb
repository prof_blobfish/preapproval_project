class JobsController < ApplicationController
  layout :resolve_layout
  before_action :authenticate_admin!, except: [:new, :create, :show]
  before_action :set_job, only: [:show, :edit, :update, :destroy]

  # GET /jobs
  # GET /jobs.json
  def index
    @fe = FormEditable.last
    @jobs = Job.all
    @person = params["person_id"]
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @fe = FormEditable.last
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Car_Approval_Results",
        template: "jobs/show.html.erb",
        locals: {:job => @job}
      end
    end
  end

  # GET /jobs/new
  def new
    @job = Job.new
    @person = params["person_id"]
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    @job.person_id = params["person_id"]

    respond_to do |format|
      if @job.save
        format.html { redirect_to person_job_path(@job.person_id, @job) }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to person_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:name, :months_at, :monthly_income, :down_payment, :person_id)
    end

    def resolve_layout
      case action_name
      when "new", "create"
        "customer_layout"
      when "show"
        "pdf"
      else
        "application"
      end
    end

end
