class PeopleController < ApplicationController
  layout :resolve_layout
  before_action :authenticate_admin!, except: [:new, :create]
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @fe = FormEditable.last
    @people = Person.all.order(created_at: :desc).limit(25)
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @fe = FormEditable.last
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.find_or_initialize_by(email: person_params[:email])
    @person.assign_attributes(person_params)

    respond_to do |format|
      if @person.save
        # UserMailer.with(person: @person).results_email.deliver_now
        format.html { redirect_to new_person_job_path(person_id: @person.id) }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:name, :phone, :email, :address, :time_at_address)
    end

    def resolve_layout
      case action_name
      when "new"
        "customer_layout"
      else
        "application"
      end

    end
end
