module PeopleHelper
  def approval_statement(approved)
    if approved
      "Pre-Approved"
    else
      "In For Review"
    end
  end

  def customer_approval_statement(approved)
    if approved
      render 'customer_pre-approved'
    else
      render 'customer_in_for_review'
    end
  end
end
