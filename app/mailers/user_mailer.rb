class UserMailer < ApplicationMailer
  default from: 'donotreply@company.com'

  def results_email
    @person = params[:person]
    @url = 'http://example.com/login'
    mail(to: @person.email, subject: 'Pre-Approval Results')
  end
end
