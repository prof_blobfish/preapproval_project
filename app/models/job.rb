class Job < ApplicationRecord
  belongs_to :form_editable
  belongs_to :person
  validates :monthly_income, :down_payment, numericality: {greater_than: 0}

  def car_price(dpp)
    down_payment / dpp
  end

  def max_payment_amount(mpal)
    monthly_income / mpal
  end

  def average_payment(dpp, m, i)
    (car_price(dpp) / m) + i
  end

  def approved?(dpp, mpal, m, i) # methods ending in "?" return true or false
    average_payment(dpp, m, i) < max_payment_amount(mpal)
  end

end
