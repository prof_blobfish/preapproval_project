json.extract! form_editable, :id, :down_payment_percentage, :max_payment_amount_limit, :months, :interest, :created_at, :updated_at
json.url form_editable_url(form_editable, format: :json)
