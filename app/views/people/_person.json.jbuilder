json.extract! person, :id, :name, :phone, :email, :address, :timeataddress, :job, :timeatjob, :income, :downpayment, :created_at, :updated_at
json.url person_url(person, format: :json)
