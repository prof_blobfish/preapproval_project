Rails.application.routes.draw do
  resources :form_editables
  devise_for :admins
  root :to => 'people#new'

  resources :people do
    resources :jobs
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
