class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :address
      t.string :timeataddress
      t.string :job
      t.string :timeatjob
      t.string :income
      t.string :downpayment

      t.timestamps
    end
  end
end
