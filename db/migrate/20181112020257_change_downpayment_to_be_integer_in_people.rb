class ChangeDownpaymentToBeIntegerInPeople < ActiveRecord::Migration[5.2]
  def change
    change_column :people, :downpayment, :integer
  end
end
