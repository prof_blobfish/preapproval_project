class ChangeIncomeToBeIntegerInPeople < ActiveRecord::Migration[5.2]
  def change
    change_column :people, :income, :integer
  end
end
