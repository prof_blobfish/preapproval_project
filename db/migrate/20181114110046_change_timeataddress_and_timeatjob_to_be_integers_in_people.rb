class ChangeTimeataddressAndTimeatjobToBeIntegersInPeople < ActiveRecord::Migration[5.2]
  def change
    change_column :people, :timeataddress, :integer
    change_column :people, :timeatjob, :integer
  end
end
