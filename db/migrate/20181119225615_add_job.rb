class AddJob < ActiveRecord::Migration[5.2]
  def change
    remove_column :people, :timeatjob
    remove_column :people, :job
    remove_column :people, :income
    remove_column :people, :downpayment
  end
end
