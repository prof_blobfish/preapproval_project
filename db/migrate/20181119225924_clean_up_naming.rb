class CleanUpNaming < ActiveRecord::Migration[5.2]
  def change
    rename_column :people, :timeataddress, :time_at_address
    drop_table :submissions
  end
end
