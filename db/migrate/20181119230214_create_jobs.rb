class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :name
      t.integer :months_at
      t.integer :monthly_income
      t.integer :down_payment

      t.timestamps
    end
  end
end
