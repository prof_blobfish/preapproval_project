class JobBelongsToPerson < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :person_id, :integer
  end
end
