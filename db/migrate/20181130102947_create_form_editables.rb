class CreateFormEditables < ActiveRecord::Migration[5.2]
  def change
    create_table :form_editables do |t|
      t.integer :down_payment_percentage
      t.integer :max_payment_amount_limit
      t.integer :months
      t.integer :interest

      t.timestamps
    end
  end
end
