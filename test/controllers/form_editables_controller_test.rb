require 'test_helper'

class FormEditablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_editable = form_editables(:one)
  end

  test "should get index" do
    get form_editables_url
    assert_response :success
  end

  test "should get new" do
    get new_form_editable_url
    assert_response :success
  end

  test "should create form_editable" do
    assert_difference('FormEditable.count') do
      post form_editables_url, params: { form_editable: { down_payment_percentage: @form_editable.down_payment_percentage, interest: @form_editable.interest, max_payment_amount_limit: @form_editable.max_payment_amount_limit, months: @form_editable.months } }
    end

    assert_redirected_to form_editable_url(FormEditable.last)
  end

  test "should show form_editable" do
    get form_editable_url(@form_editable)
    assert_response :success
  end

  test "should get edit" do
    get edit_form_editable_url(@form_editable)
    assert_response :success
  end

  test "should update form_editable" do
    patch form_editable_url(@form_editable), params: { form_editable: { down_payment_percentage: @form_editable.down_payment_percentage, interest: @form_editable.interest, max_payment_amount_limit: @form_editable.max_payment_amount_limit, months: @form_editable.months } }
    assert_redirected_to form_editable_url(@form_editable)
  end

  test "should destroy form_editable" do
    assert_difference('FormEditable.count', -1) do
      delete form_editable_url(@form_editable)
    end

    assert_redirected_to form_editables_url
  end
end
