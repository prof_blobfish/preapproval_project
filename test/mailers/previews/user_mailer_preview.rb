# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def results_email
    UserMailer.with(person: Person.last).results_email
  end
end
