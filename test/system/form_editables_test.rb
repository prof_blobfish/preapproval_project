require "application_system_test_case"

class FormEditablesTest < ApplicationSystemTestCase
  setup do
    @form_editable = form_editables(:one)
  end

  test "visiting the index" do
    visit form_editables_url
    assert_selector "h1", text: "Form Editables"
  end

  test "creating a Form editable" do
    visit form_editables_url
    click_on "New Form Editable"

    fill_in "Down Payment Percentage", with: @form_editable.down_payment_percentage
    fill_in "Interest", with: @form_editable.interest
    fill_in "Max Payment Amount Limit", with: @form_editable.max_payment_amount_limit
    fill_in "Months", with: @form_editable.months
    click_on "Create Form editable"

    assert_text "Form editable was successfully created"
    click_on "Back"
  end

  test "updating a Form editable" do
    visit form_editables_url
    click_on "Edit", match: :first

    fill_in "Down Payment Percentage", with: @form_editable.down_payment_percentage
    fill_in "Interest", with: @form_editable.interest
    fill_in "Max Payment Amount Limit", with: @form_editable.max_payment_amount_limit
    fill_in "Months", with: @form_editable.months
    click_on "Update Form editable"

    assert_text "Form editable was successfully updated"
    click_on "Back"
  end

  test "destroying a Form editable" do
    visit form_editables_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Form editable was successfully destroyed"
  end
end
